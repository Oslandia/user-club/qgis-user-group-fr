Ce projet a vocation à regrouper les demandes d'évolution de QGIS, afin de regrouper les efforts de développement et en mutualiser les financements.

L'ensemble des idées et demandes d'évolution sont [accessibles à tous](https://gitlab.com/Oslandia/user-club/qgis-user-group-fr/-/issues).

Pour participer, il faut vous connecter à ce GitLab. Vous pouvez vous [connecter sur la page de login](https://gitlab.com/users/sign_in?), en utilisant au choix : 
- un compte GitLab.com existant ou que vous créez pour l'occasion ( onglet "register" de la page )
- un compte GitHub existant
- un compte sur une de ces autres plateformes : Google, Twitter, Salesforce, Bitbucket, 

Vous êtes encouragés à : 
- [Lire les idées déjà soumises](https://gitlab.com/Oslandia/user-club/qgis-user-group-fr/-/issues)
- Contribuer aux idées déjà soumises avec vos besoins, vos remarques ( e.g. commenter sur [l'idée d'intégration d'IA dans Processing](https://gitlab.com/Oslandia/user-club/qgis-user-group-fr/-/issues/13) )
- Voter pour les idées que vous trouvez intéressantes, avec le bouton :+1: sur chaque ticket
- [créer un nouveau ticket](https://gitlab.com/Oslandia/user-club/qgis-user-group-fr/-/issues/new) pour une nouvelle idée, demande de fonctionnalité ou besoin
- Indiquer que vous êtes prêt à financer tout ou partie d'une idée