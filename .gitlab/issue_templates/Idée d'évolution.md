# [INSERER ICI NOM DE L'IDEE]

Contact : Prénom et adresse email

[[_TOC_]]

## Quel QGIS ?

### Logiciel ciblé

- [ ] Desktop
- [ ] Server

### Volet logiciel concerné

- [ ] Documentation
- [ ] Plugin
- [ ] Source de données
- [ ] Traitement

### Quelle thématique ?

- [ ] Générique

- [ ] Géologie
- [ ] Télécoms

- [ ] Autre : 

----

## Quelle idée ?

<!-- Insérer ici une description de votre idée. N'hésitez pas à être concis/e ou même verbeux/se, à ajouter des captres d'écrans ou des croquis. Bref, sentez vous libre :) -->

----

## Ressources documentaires

<!-- Liens, etc. -->



<!-- Actions rapides GitLab -->
/label ~"Idée"
